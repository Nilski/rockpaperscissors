import random
# coding: utf-8


# datorns_drag(drag) funktionen skall returnera ett slumpmässigt drag ur tupeln drag! 
# importera modulen random och använd funktionen (eg objektet) randint
def datorns_drag(drag):
    """ Skriv kod här, glöm inte att returnera :) """
    pass
 
# Kolla att draget d finns i tupeln med giltiga drag!   
def giltigt_drag(d, drag):
    """ Finns d i drag?"""
    pass

# Om ett slumptal är jämnt delbart med 6 så fuskar datorn.
# Använd randint igen
# Testa modulo operatorn '%' i Python-tolken. Den kan vara användbar?
def fusk():
    pass

# Datorn ska förklara vad som hände och varför.
def feedback(dator, spelare):
    
    # Vi antar att datorn har förlorat
    anledning = "Jag losade med en %s fan, fan farfar!" % dator

    # SEDAN TESTAR VI FALL SOM INTE STÄMMER MED VÅRT ANTAGANDE
    # Det blir Oavgjort om Datorn och spelaren körde samma drag, right?
    if dator == spelare:
        anledning = "Oavgjort, vi körde " + dator

    
    # UTTRYCK DE FALL DÅ DATORN VINNER
    
    """ uttryck sten slår sax med en if sats, använd funktionsargumenten"""
    anledning = "Yippie! Min sten krossar din sax"
    
    """ uttryck sax slår påse med en if sats, använd funktionsargumenten"""
    anledning = "Whoo hoo! sax klipper påse"
    
    """ uttryck påse slår sten med en if sats, använd funktionsargumenten"""
    anledning = "Nuu har jag dina stenar baggade! Muahh ha ha"
    
    return anledning

""" Sista uppgift: Lägg till 'sluta spela funktionalitet' och se till att programmet avbryts"""
def main():
    # drag är en tupel som innehåller möjliga drag
    drag = ('sten', 'sax', 'påse')
    
    while True:        
        sDrag = input("Vill du spela sten, sax påse? ")

        if not giltigt_drag(sDrag, drag):
            meddelande = "duh! prova skriva 'sten' 'sax' eller 'påse'! Vissa typer..."
        else:
            dDrag = datorns_drag(drag)
            meddelande = feedback(sDrag, dDrag)
        
        if fusk():
            meddelande += " ...fast JAG VANN MOT DIN %s MED EN BAZOOKA!" % sDrag.lower()

        print(meddelande)